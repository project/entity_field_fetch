# Entity Field Fetch Field (entity_field_fetch)
When you need for a field or paragraph on one node or term to be the source of content for every node on another content type, this module can get you there. It allows you to have a workflow for centralized content.


## How does it work?
Any node or term can be created to serve as the source of the centralized content.  An Entity Field Fetch field is then added to an entity (content type, vocabulary, paragraph) that should display the centralized content. The Entity Field Fetch field goes and gets (fetches) the source content from the centralized content entity and displays it on both view and edit modes for the destination entity. The source data also rides along with the destination entity in a manner similar to an entity reference so it works with entity loads and data APIs.

Example:
```
"field_cc_top_of_page_content": [
        {
            "target_type": "paragraph",
            "target_uuid": "7192f5fb-08dd-4401-a863-c5a09cc7de50",
            "target_field": null
        }

"field_cc_middle_content": [
        {
            "target_type": "node",
            "target_uuid": "65555dc-72dw-4561-j724-de59cc7dr64",
            "target_field": "field_source_middle"
        }
```

Alternate approaches?
* Blocks - Blocks can work for shared content, however their workflow is more abstract and editorial experience is not ideal.  The other drawback is that the data does not belong to the entity which can complicate decoupled approaches.
* Markup field - The data is edited in the field setting which makes workflow and revisions impossible to set up.

## Setup
1. Enable Entity Field Fetch module.
2. Create a node or term to have the field or paragraph you want to be the source of your centralized content.
3. Edit the content type that should display your centralized content.
  a. Add a field of type `Entity Field Fetch field`.
  b. In the field configuration set the:
     * “target entity type” (node or term) of the centralized content.
     * “target entity id” (nid or tid) of the centralized content.
     * “field machine name” for the field to be fetched from the centralized content.
     * if content to be fetched is in a paragraph, set the UUID of the paragraph.
4.  When you view or edit any entity that has this Entity Field Fetch field, you will see the content from the centralized content.

## Widget Settings (cache clear required after setting change)
1. The 'Show Field Label' widget setting shows/hides the form field label. Set to show the form field label by default.
2. The 'Show link to source' widget setting enables the output of a link to the source entity.
3. The 'Show source updated date' widget setting enables the output of the changed time of the source entity.

## Caching
The content displayed by an Entity Field Fetch field is cached based on the central content source. Updating the centralized content will cause the cache to be updated.


## Requirements
None.

## Caveats
* If source content is deleted, your content type that uses it breaks.

## Screenshot of Entity Field Fetch field
Example of Entity Field Fetch fields in use.
![Screenshot of example of Entity Field Fetch in use.](https://www.drupal.org/files/project-images/entity_field_fetch.png "Screenshot of example of Entity Field Fetch in use")

Example of field level config.
![Screenshot of the Entity Field Fetch field level config.](https://www.drupal.org/files/project-images/entity_field_fetch-field-config.png "Screenshot of the Entity Field Fetch field level config.")
