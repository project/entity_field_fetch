<?php

namespace Drupal\entity_field_fetch\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathValidator;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_field_fetch\Service\SourceCheck;

/**
 * Determines whether or not deletion is allowed.
 */
class DeleteAllowCheck implements AccessInterface {

  /**
   * The Entity Field Fetch source check service.
   *
   * @var Drupal\entity_field_fetch\Service\SourceCheck
   */
  protected $effSourceCheck;

  /**
   * Drupal messenger interface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The current path for the page being evaluated.
   *
   * @var Drupal\Core\Path\CurrentPathStack
   */
  protected $pathCurrent;

  /**
   * The path validatory service.
   *
   * @var Drupal\Core\Path\PathValidator
   */
  protected $pathValidator;

  /**
   * An array of routes to evaluate and check access for.
   *
   * @var array
   */
  public $routeNamesToActOn = [
    'entity.node.delete_form',
    'entity.taxonomy_term.delete_form',
  ];

  /**
   * Constructor for the access check.
   *
   * @param \Drupal\entity_field_fetch\Service\SourceCheck $source_check
   *   The EFF source check service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger interface.
   * @param \Drupal\Core\Path\CurrentPathStack $path_current
   *   The current path.
   * @param \Drupal\Core\Path\PathValidator $path_validator
   *   The messenger interface.
   */
  public function __construct(SourceCheck $source_check, MessengerInterface $messenger, CurrentPathStack $path_current, PathValidator $path_validator) {
    $this->effSourceCheck = $source_check;
    $this->messenger = $messenger;
    $this->pathCurrent = $path_current;
    $this->pathValidator = $path_validator;
  }

  /**
   * Checks access to delete form for nodes and terms.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(RouteMatchInterface $route_match, AccountInterface $account) {
    if (in_array($this->getCurrentPageRouteName(), $this->routeNamesToActOn)) {
      $parameters = $route_match->getParameters()->getIterator();
      $current_entity = reset($parameters);
      if ($current_entity instanceof ContentEntityInterface) {
        $in_use_message = $this->effSourceCheck->getIsSourceMessage($current_entity);
        if (!empty($in_use_message)) {
          $this->messenger->addWarning($in_use_message);
          return AccessResult::forbidden('Entity is a source for an entity_field_fetch field.');
        }
      }
    }

    return AccessResult::allowed();
  }

  /**
   * Get the name of the current page, not the current route access request.
   *
   * @return string
   *   The route name of the current page.
   */
  protected function getCurrentPageRouteName() {
    $current_path = $this->pathCurrent->getPath();
    $url_object = $this->pathValidator->getUrlIfValidWithoutAccessCheck($current_path);
    $route_name = $url_object->getRouteName();
    return $route_name;
  }

}
