<?php

namespace Drupal\entity_field_fetch\Service;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Render\FilteredMarkup;

/**
 * Class to fetch fields and paragraphs.
 */
class Fetcher {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $fieldDefinition = NULL;

  /**
   * The target entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity = NULL;

  /**
   * The simplified data that gets returned as "fetched".
   *
   * @var array
   */
  protected $fetchedData = [];

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The drupal messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The paragraph entity if needed.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $paragraph = NULL;

  /**
   * Optional paragraph id or UUID.
   *
   * @var string
   */
  protected $paragraphId = NULL;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer = NULL;

  /**
   * Constructs a new Fetcher object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger interface.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer, LoggerChannelFactoryInterface $logger, MessengerInterface $messenger) {
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->fetchedData = [];
  }

  /**
   * Sets the field definition and initializes everything.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition.
   */
  public function setFieldDefinition(FieldDefinitionInterface $field_definition) {
    $this->nuke();
    $this->fieldDefinition = $field_definition;
    $this->init();
  }

  /**
   * Removes all data that should not be kept in state.
   */
  protected function nuke() {
    $this->fieldDefinition = NULL;
    $this->entity = NULL;
    $this->paragraph = NULL;
    $this->paragraphId = NULL;
    $this->fetchedData = [];
  }

  /**
   * Init call to start assembling the data.
   */
  protected function init() {
    $this->haveDefinition();
  }

  /**
   * Checks to see if the definition has been set.
   *
   * @throws \Exception
   *   If definition has not been set.
   */
  protected function haveDefinition() {
    if (empty($this->fieldDefinition)) {
      // We don't have the definition throw an exception.
      throw new \Exception("entity_field_fetch Fetcher service requires setFieldDefinition() be called first.");
    }
  }

  /**
   * Getter and setter for the Paragraph id.
   *
   * @return string
   *   The UUID or ID of the paragraph. Normalized to ID loadParagraph.
   */
  public function getParagraphId() {
    if (empty($this->paragraphId)) {
      $this->haveDefinition();
      $this->paragraphId = $this->fieldDefinition->getSetting('target_paragraph_uuid');
    }
    return $this->paragraphId;
  }

  /**
   * Get the target entity type from field settings.
   *
   * @return string
   *   The target entity type (node, term, or paragraph).
   */
  public function getTargetEntityType() {
    $this->haveDefinition();
    $type = $this->fieldDefinition->getSetting('target_entity_type');
    $type = (empty($this->getParagraphId())) ? $type : 'paragraph';
    return $type;
  }

  /**
   * Get the target id from field settings.
   *
   * @return string
   *   The target id (nid, tid, or pid) for the target.
   */
  public function getTargetId() {
    $this->haveDefinition();
    $target_entity_id = $this->fieldDefinition->getSetting('target_entity_id');
    // Use the id that matches the type.
    return ($this->getTargetEntityType() === 'paragraph') ? $this->getParagraphId() : $target_entity_id;
  }

  /**
   * Gets the target field name if it is appropriate.
   *
   * @return string|null
   *   String if the fieldname is used, NULL if a paragraph is the target.
   */
  public function getTargetFieldName() {
    $this->haveDefinition();
    $target_fieldname = $this->fieldDefinition->getSetting('field_to_fetch');
    // In the event a paragraph is the target, ignore the field as irrelevant.
    return ($this->getTargetEntityType() === 'paragraph') ? NULL : $target_fieldname;
  }

  /**
   * Triggers the load of primary entity and optionally the target paragraph.
   */
  protected function loadEntities() {
    $this->haveDefinition();
    // Only load if they have not been previously loaded.
    if (empty($this->entity) && empty($this->paragraph)) {
      $this->loadPrimaryEntity();
      if ($this->getTargetEntityType() === 'paragraph') {
        $this->loadParagraph();
      }
    }
  }

  /**
   * Loads the primary entity and sets it.
   */
  protected function loadPrimaryEntity() {
    // Load up the source info.
    $target_entity_type = $this->fieldDefinition->getSetting('target_entity_type');
    $target_entity_id = $this->fieldDefinition->getSetting('target_entity_id');
    $this->entity = $this->entityTypeManager->getStorage($target_entity_type)->load($target_entity_id);
  }

  /**
   * Loads the paragraph and sets it.
   */
  protected function loadParagraph() {
    if (Uuid::isValid($this->getParagraphId())) {
      // This is a valid uuid so load the paragraph by id.
      // Support for UUID is only here for backward compatibility.
      $this->paragraph = $this->entityRepository->loadEntityByUuid('paragraph', $this->getParagraphId());
      $this->paragraphId = $this->paragraph->id();
    }
    else {
      // This is an id, so load it from an id.
      // How to build context.
      $this->paragraph = $this->entityRepository->getCanonical('paragraph', $this->getParagraphId());
    }
  }

  /**
   * Get the bundle of the target entity.
   *
   * @return string
   *   The bundle id of of the node, term, or paragraph.
   */
  public function getFetchedBundle() {
    $this->loadEntities();
    if (!empty($this->paragraph)) {
      $bundle = $this->paragraph->bundle();
    }
    else {
      $bundle = $this->entity->bundle();
    }

    return $bundle;
  }

  /**
   * Gets the field values from the target entity.
   *
   * @return array
   *   All field values keyed by field name.
   */
  public function getEntityData() {
    $this->loadEntities();
    if ($this->getTargetEntityType() === 'paragraph') {
      $data = (empty($this->paragraph)) ? [] : $this->paragraph->toArray();
      $this->fetchedData = $this->removeNonFields($data);
    }
    else {
      // Need to dig the field out of the entity.
      $entity_data = $this->entity->toArray();
      $this->fetchedData[$this->getTargetFieldName()] = $entity_data[$this->getTargetFieldName()] ?? [];
    }
    $source = ($this->getTargetEntityType() === 'paragraph') ? $this->paragraph : $this->entity;
    $this->fetchedData = $this->processFields($this->fetchedData, $source, 0);

    return $this->fetchedData;
  }

  /**
   * Initiates special processing for specific field types.
   *
   * @param array $data
   *   Array of data fields by reference.
   * @param object $source
   *   The source entity.
   * @param int $level
   *   The depth level.  Intended to prevent digging too deep.
   *
   * @return array
   *   Array of processed field data from the source entity.
   */
  protected function processFields(array $data, $source, $level) {
    // Loop through fields and subprocess them.
    // Assuming if there is digging beyond level 10 something has gone wrong.
    if ($level <= 10 && $source !== NULL) {
      foreach ($data as $fieldname => $values) {
        $this->addProcessedToTextFormatFields($data, $source, $fieldname, $values);
        $this->extractEntityReference($data, $source, $fieldname, $level);
      }
    }
    return $data;
  }

  /**
   * Adds a processed attribute to any field value that needs it.
   *
   * @param array $data
   *   Array of data fields by reference.
   * @param object $source
   *   The source object (paragraph, node, term).
   * @param string $fieldname
   *   The field name of what is being processed.
   * @param string|int|null|array $values
   *   The values from the entity property or field.  Only operated on if array.
   */
  protected function addProcessedToTextFormatFields(array &$data, object $source, string $fieldname, $values) {
    if (is_array($values)) {
      foreach ($values as $index => $instanceValues) {
        if (!empty($instanceValues['value']) && !empty($instanceValues['format'])) {
          // Best guess is this looks like a wysiwyg field so process.
          $field = $source->get($fieldname)->get($index);
          $build = $field->view();
          $processed_text = $this->renderer->renderPlain($build);
          $processed = FilterProcessResult::createFromRenderArray($build)->setProcessedText((string) $processed_text);
          $data[$fieldname][$index]['processed'] = FilteredMarkup::create($processed->getProcessedText());
        }
      }
    }
  }

  /**
   * Grab the entire referenced entity data.
   *
   * @param array $data
   *   Array of data fields by reference.
   * @param \Drupal\Core\Entity\ContentEntityInterface $source
   *   The source object (paragraph, node, term).
   * @param string $fieldname
   *   The field name of what is being processed.
   * @param int $level
   *   The depth level on the dig into entities.
   */
  protected function extractEntityReference(array &$data, ContentEntityInterface $source, $fieldname, $level) {
    $level++;
    $field_definition = $source->getFieldDefinition($fieldname);
    if (empty($field_definition)) {
      // The field does not seem to exist.  May have been removed or typo.
      return;
    }

    $field_type = $source->getFieldDefinition($fieldname)->getType();

    $referenced_entities = ($this->isEntityReference($field_type)) ? $source->get($fieldname)->referencedEntities() : [];
    if (is_array($referenced_entities)) {
      foreach ($referenced_entities as $index => $referenced_entity) {
        if ($referenced_entity instanceof ContentEntityInterface) {
          $referenced_entity_data = $referenced_entity->toArray();
          $referenced_entity_data = $this->removeNonFields($referenced_entity_data);
          // Add some missing metadata that got stripped out.
          $data[$fieldname][$index]['type'] = $referenced_entity->getEntityTypeId();
          $data[$fieldname][$index]['bundle'] = $referenced_entity->bundle();

          // Some content type specific handling.
          switch ($referenced_entity->getEntityTypeId()) {
            case 'node':
              $idkey = 'nid';
              $data[$fieldname][$index]['url'] = $referenced_entity->toUrl()->toString();
              break;

            case 'term':
              $idkey = 'tid';
              $data[$fieldname][$index]['url'] = $referenced_entity->toUrl()->toString();
              break;

            case 'paragraph':
              $idkey = 'pid';
              break;

            default:
              $idkey = 'id';
              break;
          }
          $data[$fieldname][$index][$idkey] = $referenced_entity->id();
          $data[$fieldname][$index]['label'] = $referenced_entity->label();
          $data[$fieldname][$index]['status'] = $referenced_entity->isPublished();
          $data[$fieldname][$index]['langcode'] = $referenced_entity->language()->getId();
          $data[$fieldname][$index] = array_merge($data[$fieldname][$index], $this->processFields($referenced_entity_data, $referenced_entity, $level));
        }
      }
    }
  }

  /**
   * Checks for field type being classified as entity reference.
   *
   * @param string $field_type
   *   The field type to check.
   *
   * @return bool
   *   TRUE if is a field_type that is an entity reference.  FALSE otherwise.
   */
  protected function isEntityReference(string $field_type) {
    $entity_reference_types = ['entity_reference_revisions'];
    return (in_array($field_type, $entity_reference_types)) ? TRUE : FALSE;
  }

  /**
   * Remove any entity properties that are not fields.
   *
   * @param array $data
   *   The data array containing all entity properties.
   *
   * @return array
   *   The data with any non-fields removed.
   */
  protected function removeNonFields(array $data) {
    foreach ($data as $name => $values) {
      if (stripos($name, 'field') !== 0) {
        unset($data[$name]);
      }
    }
    return $data;
  }

  /**
   * The cache contexts related to this fetched item.
   *
   * @return array
   *   The cache contexts for the source entity.
   */
  public function getCacheContexts() {
    $this->haveDefinition();
    $this->loadEntities();
    $contexts = $this->entity->getCacheContexts() ?? $this->paragraph->getCacheContexts() ?? [];
    array_merge($contexts, ['languages']);
    return $contexts;
  }

  /**
   * The cache keys that uniquely identify this content.
   *
   * @param array $additional_keys
   *   Optional array of keys to add to the returned keys.
   *
   * @return array
   *   An array of keys that uniquely identify this fetched item.
   */
  public function getCacheKeys(array $additional_keys = []) {
    $this->haveDefinition();
    $keys = [
      $this->fieldDefinition->getName(),
      $this->getTargetEntityType(),
      $this->getTargetId(),
    ];

    $keys = array_merge($keys, $additional_keys);
    return $keys;
  }

  /**
   * Gets the cache tags to use for this fetched content.
   *
   * @return array
   *   The array of cache tags.
   */
  public function getCacheTags() {
    // The cache should be expired when the source entity is updated.
    return [
      "{$this->getTargetEntityType()}:{$this->getTargetId()}",
    ];
  }

}
