<?php

namespace Drupal\entity_field_fetch\Service;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\TranslationManager;

/**
 * Class to Check if an entity is used as a source of content (target) for EFF.
 */
class SourceCheck {

  /**
   * An array of EFF sources, or null if they have not been collected yet.
   *
   * @var array|null
   */
  protected $effSources = NULL;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $translationManager;

  /**
   * Constructs a new SourceCheck object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\StringTranslation\TranslationManager $translation_manager
   *   The messenger interface.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, TranslationManager $translation_manager) {
    $this->entityFieldManager = $entity_field_manager;
    $this->translationManager = $translation_manager;
  }

  /**
   * Evaluate if the source is used and return message if it is.
   *
   * @param \Drupal\Core\Entity\EntityInterface $current_entity
   *   The entity being checked for being an EFF source.
   *
   * @return string|null
   *   String of warning message to use if it is an EFF source, NULL if not.
   */
  public function getIsSourceMessage(EntityInterface $current_entity) {
    $current_entity_id = $current_entity->id();
    $current_entity_type = $current_entity->getEntityType()->id();

    return $this->buildSourceMessages($current_entity_type, $current_entity_id);
  }

  /**
   * Builds and returns an array of arrays of EFF sources (targets).
   *
   * @return array
   *   A multidimensional array of EFF sources and the fields that target them.
   */
  protected function getEffSources() {
    if (is_null($this->effSources)) {
      // Collect the targets.
      $sources = [];
      $eff_entity_instances = $this->entityFieldManager->getFieldMapByFieldType('entity_field_fetch');
      foreach ($eff_entity_instances as $type => $eff_fields) {
        foreach ($eff_fields as $field_name => $eff_field) {
          $bundle = reset($eff_field['bundles']);
          $definitions = $this->entityFieldManager->getFieldDefinitions($type, $bundle);
          if (isset($definitions[$field_name])) {
            $source_id = $definitions[$field_name]->getSetting('target_entity_id');
            $source_type = $definitions[$field_name]->getSetting('target_entity_type');
            // Pattern 'source_type:id' => [EFF content type:bundle[fields]].
            $sources["{$source_type}:{$source_id}"]["{$type}:{$bundle}"]['fields'][] = $field_name;
          }
        }
      }
      // They should not change in an any given load, so keep them in state for
      // repeated lookups from other calls to the service.
      $this->effSources = $sources;
    }

    return $this->effSources;
  }

  /**
   * Build and return any messaging declaring this entity is an EFF source.
   *
   * @param string $current_entity_type
   *   The current entity type (node, taxonomy_term).
   * @param string $current_entity_id
   *   Node or term id ("123").
   *
   * @return string|null
   *   A translated message string that lists the entity types that treat this
   *   entity as an EFF source.  NULL this entity is not a source.
   */
  protected function buildSourceMessages($current_entity_type, $current_entity_id) {
    if ($this->isEffSource($current_entity_type, $current_entity_id)) {
      $uses = [];
      $instances = $this->effSources["{$current_entity_type}:{$current_entity_id}"];
      foreach ($instances as $nodebundle => $instance) {
        $fields = array_unique($instance['fields'], SORT_REGULAR);
        $fields_concat = implode(', ', $fields);
        $uses[$nodebundle] = $fields_concat;
      }
      return $this->translationManager->translate(
      'Cannot delete %type: %id because it is used as a source by the following entity_field_fetch instances: %fields',
       [
         '%type' => $current_entity_type,
         '%id' => $current_entity_id,
         '%fields' => json_encode($uses, JSON_PRETTY_PRINT),
       ]);
    }
    return NULL;
  }

  /**
   * Checks if the current entity is an EFF source (target) for the field.
   *
   * @param string $current_entity_type
   *   Then entity type (node or taxonomy_term).
   * @param string $current_entity_id
   *   The tid or nid of the current entity.
   *
   * @return bool
   *   TRUE if the current entity is an EFF source (target) for the field.
   */
  protected function isEffSource($current_entity_type, $current_entity_id) {
    if (isset($this->getEffSources()["{$current_entity_type}:{$current_entity_id}"])) {
      return TRUE;
    }
    return FALSE;
  }

}
