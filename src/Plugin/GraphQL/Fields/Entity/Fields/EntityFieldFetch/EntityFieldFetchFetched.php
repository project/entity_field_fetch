<?php

namespace Drupal\entity_field_fetch\Plugin\GraphQL\Fields\Entity\Fields\EntityFieldFetch;

use Drupal\Core\Url;
use Drupal\entity_field_fetch\Plugin\Field\FieldType\EntityFieldFetchItem;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\graphql\Utility\StringHelper;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Resolve fetched Data to look like GraphQL.
 *
 * @GraphQLField(
 *   id = "fetched",
 *   secure = true,
 *   name = "fetched",
 *   type = "Map",
 *   provider = "entity_field_fetch",
 *   field_types = {"entity_field_fetch"},
 *   deriver = "Drupal\graphql_core\Plugin\Deriver\Fields\EntityFieldPropertyDeriver"
 * )
 */
class EntityFieldFetchFetched extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof EntityFieldFetchItem) {
      $newarray = $this->convertKeysToCamel($value->fetched);
      $this->addPathToUri($newarray);

      yield $newarray;
    }
  }

  /**
   * Add url::path to fields where uri is present.
   *
   * @param mixed $item_to_process
   *   An element from a fetched field by reference.
   */
  protected function addPathToUri(&$item_to_process) {
    if (is_array($item_to_process)) {
      foreach ($item_to_process as $key => &$value) {
        if ($key === 'uri' && !empty($value) && empty($item_to_process['url'])) {
          $path = Url::fromUri($value)->toString();
          // Add an url with path.
          if (!empty($path)) {
            $item_to_process['url'] = ['path' => $path];
          }
        }
        $this->addPathToUri($value);

      }
      // Must unset the reference from the loop.
      unset($value);
    }
  }

  /**
   * Alter array keys to camel case. Can be called recursively.
   *
   * @param mixed $item_to_process
   *   Arrays get processed, all other types pass through.
   *
   * @return mixed
   *   Returns the same kind of item that was passed in.
   */
  protected function convertKeysToCamel($item_to_process) {
    if (is_array($item_to_process)) {
      $new_array = [];
      foreach ($item_to_process as $key => $value) {
        $new_key = (is_numeric($key)) ? $key : StringHelper::propCase($key);
        $new_key = $this->mapKey($new_key);
        $new_array[$new_key] = $this->convertKeysToCamel($value);
      }

      if (!empty($item_to_process['type'])) {
        // Assuming this is an entity.
        $entity_wrapper = [
          'entity' => $new_array,
        ];

        return $entity_wrapper;
      }
      return $new_array;
    }
    else {
      return $item_to_process;
    }
  }

  /**
   * Does key replacement of keys differ between Drupal and GraphQL.
   *
   * @param mixed $original_key
   *   The original key.
   *
   * @return mixed
   *   The mapped key if there is one, or the original key.
   */
  protected function mapKey($original_key) {
    $key_map = [
      // Drupal key => graphQL key.
      'bundle' => 'entityBundle',
      'type' => 'entityType',

    ];
    return (!empty($key_map[$original_key])) ? $key_map[$original_key] : $original_key;

  }

}
