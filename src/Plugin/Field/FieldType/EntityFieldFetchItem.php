<?php

namespace Drupal\entity_field_fetch\Plugin\Field\FieldType;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;

/**
 * Provides a field type of EntityFieldFetch.
 *
 * @FieldType(
 *   id = "entity_field_fetch",
 *   label = @Translation("Entity Field Fetch field"),
 *   description = @Translation("This field TYPE, not item, stores the target information needed to fetch field data from a single source."),
 *   default_formatter = "entity_field_fetch",
 *   default_widget = "entity_field_fetch_widget",
 * )
 */
class EntityFieldFetchItem extends FieldItemBase implements CacheableDependencyInterface {

  /**
   * The field's definition.
   *
   * @var \Drupal\Core\TypedData\DataDefinitionInterface
   */
  public $definition;

  /**
   * The fetcher service.
   *
   * @var \Drupal\entity_field_fetch\Service\Fetcher
   */
  public $fetcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);
    // Have to use this, Symfony create() dependency injection doesn't fire.
    /** @var \Drupal\entity_field_fetch\Service\Fetcher */
    $fetcher = \Drupal::service('entity_field_fetch.fetcher');
    $this->fetcher = $fetcher;
    $this->definition = $definition;
    $this->fetcher->setFieldDefinition($this->definition->getFieldDefinition());
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    // This field should store nothing on the node but Drupal must have a col.
    return [
      'columns' => [
        'value' => [
          'type' => 'int',
          'size' => 'tiny',
          'description' => "Not needed but required by Drupal.",
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    // Drupal requires a value to be saved, even though the relevant properties
    // are all computed.  So we save the smallest thing.
    $properties['value'] = DataDefinition::create('boolean')
      ->setLabel(t('unused value'))
      ->setInternal(TRUE)
      ->setDescription(t('unused value'));

    $properties['target_type'] = DataDefinition::create('string')
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setLabel(t('Entity type'))
      ->setDescription(t('Node or Term'));

    $properties['target_id'] = DataDefinition::create('string')
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setLabel(t('Target entity ID'))
      ->setDescription(t('The id of the target entity.'));

    $properties['target_field'] = DataDefinition::create('string')
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setLabel(t('Target field'))
      ->setDescription(t('The machine name of the field to pull from.'));

    $properties['fetched_bundle'] = DataDefinition::create('string')
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setLabel(t('Fetched bundle'))
      ->setDescription(t('The bundle of the fetched entity.'));

    $properties['fetched'] = DataDefinition::create('any')
      ->setLabel(t('Fetched content'))
      ->setDescription(t('The data of the fetched item.'))
      ->setComputed(TRUE)
      ->setInternal(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // All values are calculated and as a result are never empty.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      // Declare the settings with defaults.
      'target_entity_type' => '',
      'target_entity_id' => '',
      'field_to_fetch' => '',
      'target_paragraph_uuid' => '',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $element['target_entity_type'] = [
      '#title' => $this->t('Target entity type'),
      '#type' => 'select',
      '#options' => [
        'node' => $this->t('Node'),
        'term' => $this->t('Term'),
      ],
      '#required' => FALSE,
      '#default_value' => $this->getSetting('target_entity_type'),
    ];

    $element['target_entity_id'] = [
      '#title' => $this->t('Target entity id @examples.', ['@examples' => '(nid or tid)']),
      '#type' => 'number',
      '#size' => 36,
      '#min' => 1,
      '#required' => TRUE,
      '#default_value' => $this->getSetting('target_entity_id'),
    ];

    $element['field_to_fetch'] = [
      '#title' => $this->t('Machine name of field to fetch.'),
      '#type' => 'textfield',
      '#size' => 32,
      '#maxlength' => 32,
      '#required' => TRUE,
      '#default_value' => $this->getSetting('field_to_fetch'),
    ];

    $element['target_paragraph_uuid'] = [
      '#title' => $this->t('Paragraph ID or UUID to fetch (optional)'),
      '#type' => 'textfield',
      '#size' => 36,
      '#maxlength' => 36,
      '#required' => FALSE,
      '#default_value' => $this->getSetting('target_paragraph_uuid'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    $this->fetcher->setFieldDefinition($this->definition->getFieldDefinition());
    // Assemble the values.
    $values['target_type'] = $this->fetcher->getTargetEntityType();
    // Use the id that matches the type.
    $values['target_id'] = $this->fetcher->getTargetId();
    // If it is a paragraph, do not reference the field.
    $values['target_field'] = $this->fetcher->getTargetFieldName();
    $values['fetched_bundle'] = $this->fetcher->getFetchedBundle();
    $values['fetched'] = $this->fetcher->getEntityData();

    parent::setValue($values, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // Returns a array of cache context tokens, used to generate a cache ID.
    $contexts = $this->fetcher->getCacheContexts();
    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // Return an array tags invalidate when this updates.
    return $this->fetcher->getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // The maximum time in seconds that this object may be cached.
    return 5000;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'fetched';
  }

  /*
   * Informs the plugin that a dependency of the field will be deleted.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $dependencies
   *   An array of dependencies that will be deleted keyed by dependency type.
   *   Dependency types are, for example, entity, module and theme.
   *
   * @return bool
   *   TRUE if the field definition has been changed as a result, FALSE if not.
   *
   * @see \Drupal\Core\Config\ConfigEntityInterface::onDependencyRemoval()
   *
   * @todo Work this out.
   *  public static function onDependencyRemoval(FieldDefinitionInterface
   * $field_definition, array $dependencies);
   */

}
