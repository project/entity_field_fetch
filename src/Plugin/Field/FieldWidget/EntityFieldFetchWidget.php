<?php

namespace Drupal\entity_field_fetch\Plugin\Field\FieldWidget;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_field_fetch\Service\Fetcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A widget for EntityFieldFetch fields.
 *
 * @FieldWidget(
 *   id = "entity_field_fetch_widget",
 *   label = @Translation("Entity Field Fetch widget"),
 *   field_types = {
 *     "entity_field_fetch"
 *   }
 * )
 */
class EntityFieldFetchWidget extends WidgetBase implements WidgetInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;


  /**
   * The field definition interface.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $definition;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field fetch fetcher service.
   *
   * @var \Drupal\entity_field_fetch\Service\Fetcher
   */
  protected $fetcher;

  /**
   * Constructs a EntityFieldFetchWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity manager service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   Entity Repository.
   * @param \Drupal\entity_field_fetch\Service\Fetcher $fetcher
   *   Field Fetch fetcher service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, DateFormatterInterface $date_formatter, EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository, Fetcher $fetcher) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
    $this->dateFormatter = $date_formatter;
    $this->definition = $field_definition;
    $this->fetcher = $fetcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      // Add any services we want to inject.
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('entity_field_fetch.fetcher')
      );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Create the custom settings and set defaults.
      'show_field_label' => TRUE,
      'show_link_to_source' => FALSE,
      'show_source_updated_date' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['show_field_label'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show field label'),
      '#default_value' => $this->getSetting('show_field_label'),
      '#weight' => -1,
    ];
    $element['show_link_to_source'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show link to source entity.'),
      '#default_value' => $this->getSetting('show_link_to_source'),
      '#weight' => 1,
    ];
    $element['show_source_updated_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show source updated date.'),
      '#default_value' => $this->getSetting('show_source_updated_date'),
      '#weight' => 2,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    // If Show Field Label widget settings unchecked then display No in summary,
    // otherwise show Yes.
    $show_value = ($this->getSetting('show_field_label')) ? $this->t('Yes') : $this->t('No');
    $summary[] = $this->t('Show field label: @show_field_label', ['@show_field_label' => $show_value]);

    $show_value = ($this->getSetting('show_link_to_source')) ? $this->t('Yes') : $this->t('No');
    $summary[] = $this->t('Show link to source: @show_link_to_source', ['@show_link_to_source' => $show_value]);

    $show_value = ($this->getSetting('show_source_updated_date')) ? $this->t('Yes') : $this->t('No');
    $summary[] = $this->t('Show source updated date: @show_source_updated_date', ['@show_source_updated_date' => $show_value]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $target_entity_type = $this->fieldDefinition->getSetting('target_entity_type');
    $target_entity_id = $this->fieldDefinition->getSetting('target_entity_id');
    $target_fieldname = $this->fieldDefinition->getSetting('field_to_fetch');
    // This could be an id or UUID (for backwards compatibility).
    $target_paragraph_id = $this->fieldDefinition->getSetting('target_paragraph_uuid');
    if (empty($target_entity_type) || empty($target_entity_id)) {
      // The field has no data yet so create a preview notice.
      return [
        '#type' => 'markup',
        '#markup' => $this->t('[ No preview exists yet as the field is not configured. ]'),
      ];
    }

    $element['#type'] = 'item';
    $element['#suffix'] = (!empty($element['#suffix'])) ? $element['#suffix'] : '';
    $source_entity_type = (empty($target_paragraph_id)) ? $target_entity_type : 'paragraph';

    // If Show Field Label widget settings unchecked then hide label.
    $element['#title_display'] = ($this->getSetting('show_field_label')) ? 'before' : 'invisible';

    // Load up the source info.
    $entity = $this->entityTypeManager->getStorage($target_entity_type)->load($target_entity_id);

    if ($source_entity_type === 'paragraph') {
      // Determine if we have an id or uuid.
      if (Uuid::isValid($target_paragraph_id)) {
        // This is a valid uuid so load the paragraph by id.
        // Support for UUID is only here for backward compatibility.
        $paragraph = $this->entityRepository->loadEntityByUuid('paragraph', $target_paragraph_id);
        $target_paragraph_id = $paragraph->id();
      }
      else {
        // This is an id, so load it from an id.
        $paragraph = $this->entityRepository->getCanonical('paragraph', $target_paragraph_id);
      }

      if (!empty($paragraph)) {
        // Since we are getting the target entity directly, swap out target id.
        $target_paragraph_id = $paragraph->id();
        $target_render = $this->entityTypeManager->getViewBuilder('paragraph')->view($paragraph, 'full');
      }
      else {
        $element = [
          '#type' => 'markup',
          '#markup' => $this->t('A Paragraph with an id = @id could not be found.  Please check your field settings.', ['@id' => $target_paragraph_id]),
        ];
      }

    }
    else {
      // The data is in the source entity render it.
      if (!empty($entity->$target_fieldname)) {
        $target_render = $entity->$target_fieldname->view('full');
      }
      else {
        $element = [
          '#type' => 'markup',
          '#markup' => $this->t('The field @id could not be found.  Please check your field settings.', ['@id' => $target_fieldname]),
        ];
      }
    }

    if ($this->getSetting('show_link_to_source') && $this->getSetting('show_source_updated_date') && !empty($entity)) {
      // Show the date in the link.
      $updated_date = (!empty($entity)) ? $this->dateFormatter->format($entity->getChangedTime(), 'short') : '?';
      $text = $this->t("Updated: @updated", ['@updated' => $updated_date]);
      $options = [
        'attributes' => [
          'title' => $this->t('Link to the source.'),
        ],
      ];
      $link = $entity->toLink($text, 'canonical', $options);
      $source_link = $link->toString();
      $element['#suffix'] .= "<div class=\"data-source data-link data-updated\">{$source_link}</div>";

    }
    elseif ($this->getSetting('show_link_to_source') && !empty($entity)) {
      // Show the link.
      $link = $entity->toLink();
      $link->setText('Source');
      $source_link = $link->toString();
      $element['#suffix'] .= "<div class=\"data-source data-link\">{$source_link}</div>";
    }
    elseif ($this->getSetting('show_source_updated_date') && !empty($entity)) {
      // Show updated date.
      $updated_date = $this->dateFormatter->format($entity->getChangedTime(), 'short');
      $updated = $this->t("Updated: @updated", ['@updated' => $updated_date]);
      $element['#suffix'] .= "<div class=\"data-source data-updated\">{$updated}</div>";
    }

    if (!empty($entity)) {
      // Enable caching for speed and uniformity.
      $this->fetcher->setFieldDefinition($this->definition);
      $element['#cache'] = [
        'keys' => $this->fetcher->getCacheKeys(['edit']),
        'contexts' => $this->fetcher->getCacheContexts(),
        'tags' => $this->fetcher->getCacheTags(),
      ];
    }

    // Include the render array for the fetched content.
    $element['fetched_source'] = $target_render ?? [];
    $element['fetched_source']['#weight'] = 10;
    // Only including this for parity with schema. Otherwise an error appears.
    $element['value'] = [
      '#type' => 'hidden',
      '#disabled' => TRUE,
      '#size' => 1,
      '#value' => 0,
      '#weight' => 2,
    ];

    return $element;
  }

}
