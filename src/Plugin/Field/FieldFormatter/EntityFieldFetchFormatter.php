<?php

namespace Drupal\entity_field_fetch\Plugin\Field\FieldFormatter;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_field_fetch\Service\Fetcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'EntityFieldFetch' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_field_fetch",
 *   label = @Translation("Entity Field Fetch"),
 *   field_types = {
 *     "entity_field_fetch"
 *   }
 * )
 */
class EntityFieldFetchFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The entity field fetch fetcher service.
   *
   * @var \Drupal\entity_field_fetch\Service\Fetcher
   */
  protected $fetcher;

  /**
   * The field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $fieldDefinition;

  /**
   * Construct a MyFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Defines an interface for entity field definitions.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity manager service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   Entity Repository.
   * @param \Drupal\entity_field_fetch\Service\Fetcher $fetcher
   *   Field Fetch fetcher service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository, Fetcher $fetcher) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->fieldDefinition = $field_definition;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
    $this->fetcher = $fetcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      // Add any services you want to inject here.
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('entity_field_fetch.fetcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays the fetched field/paragraph.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewElement($item, $langcode);
    }

    return $elements;
  }

  /**
   * Builds a renderable array for fetched targets.
   *
   * @param object $fetchField
   *   The Fetch field.
   * @param string $langcode
   *   The language that should be used to render the field.
   *
   * @return array
   *   A renderable array.
   */
  protected function viewElement($fetchField, $langcode) {
    $field_name = $this->fieldDefinition->getName();
    $target_entity_type = $this->fieldDefinition->getSetting('target_entity_type');
    $target_entity_id = $this->fieldDefinition->getSetting('target_entity_id');
    $target_fieldname = $this->fieldDefinition->getSetting('field_to_fetch');
    // This can be an id or a UUID.
    $target_paragraph_id = $this->fieldDefinition->getSetting('target_paragraph_uuid');

    // Load up the source entity.
    if (!empty($target_entity_type) && !empty($target_entity_id)) {
      $entity = $this->entityTypeManager->getStorage($target_entity_type)->load($target_entity_id);
      if (empty($entity)) {
        // We have no entity to fetch so render some info.
        $vars = [
          '@field_name' => $field_name,
          '@type' => $target_entity_type,
          '@id' => $target_entity_id,
        ];
        return [
          '#type' => 'markup',
          '#markup' => $this->t('The field @field_name could not load a @type with an id of @id. Please check your field settings.', $vars),
        ];
      }
    }
    else {
      $vars = [
        '@field_name' => $field_name,
      ];
      return [
        '#type' => 'markup',
        '#markup' => $this->t('The field @field_name must have both an entity type and id defined in the settings. Please check your field settings.', $vars),
      ];
    }

    if (!empty($target_paragraph_id)) {
      // Ignore the node, the data we want is all in the related paragraph.
      // Determine if we have an id or uuid.
      if (Uuid::isValid($target_paragraph_id)) {
        // This is a valid uuid so load the paragraph by id.
        // Support for UUID is only here for backward compatibility.
        $paragraph = $this->entityRepository->loadEntityByUuid('paragraph', $target_paragraph_id);
      }
      else {
        // This is an id, so load it from an id.
        $paragraph = $this->entityRepository->getCanonical('paragraph', $target_paragraph_id);
      }

      if (!empty($paragraph)) {
        // Normalize on the paragraph id.
        $target_paragraph_id = $paragraph->id();
        $builder = $this->entityTypeManager->getViewBuilder('paragraph');
        $element = $builder->view($paragraph, $this->viewMode);
      }
      else {
        $element = [
          '#type' => 'markup',
          '#markup' => $this->t('A Paragraph with an id = @id could not be found.  Please check your field settings.', ['@id' => $target_paragraph_id]),
        ];
      }
    }
    else {
      // We are looking for just the field to render.
      $element = $entity->$target_fieldname->view($this->viewMode);
    }
    $this->fetcher->setFieldDefinition($this->fieldDefinition);
    $element['#cache'] = [
      'contexts' => $this->fetcher->getCacheContexts(),
      'keys' => $this->fetcher->getCacheKeys(['formatter']),
      'tags' => $this->fetcher->getCacheTags(),
    ];
    // Handle the unpublished indicator.
    if (!$entity->isPublished()) {
      $element['#prefix'] = '<div class="node--unpublished">';
      $element['#suffix'] = '</div>';
    }

    return $element;
  }

}
