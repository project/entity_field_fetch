<?php

namespace Drupal\entity_field_fetch\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Monitors node and term delete routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Check for Entity Field Fetch use before deletion of terms or nodes.
    if ($route = $collection->get('entity.node.delete_form')) {
      $route->setRequirement('_eff_delete_allow_check', 'entity_field_fetch.delete_allow_check::access');
    }
    if ($route = $collection->get('entity.taxonomy_term.delete_form')) {
      $route->setRequirement('_eff_delete_allow_check', 'entity_field_fetch.delete_allow_check::access');
    }
  }

}
